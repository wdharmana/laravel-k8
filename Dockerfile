 FROM php:7.4-apache
 MAINTAINER Neo Ighodaro <neo@hotels.ng>
 COPY . /var/www/
 RUN chmod -Rf 777 /var/www/storage/
#  RUN cd /var/www/ && composer install --no-dev

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# COPY . /var/www/tmp
RUN cd /var/www && composer install --no-dev
